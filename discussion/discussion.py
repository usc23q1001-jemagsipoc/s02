# Python Syntax of hello world in Python
print("Hello World")

# Indentation
# Where in other programming languages the indentation in code is for readability, the indentation in Python is VERY IMPORTANT.

# Variables
# Declaring Variables
age = 18
middle_inital = "C"
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# Strings(str) - for alphanumberic and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# Numbers (int, float, complex)
num_of_days = 365 #this is an integer
pi_approx = 3.1416
complex_num = 1 + 5j

# Boolean (bool) - for truth values
isLearning = True
isDifficult = False

# Using Vatiables
print("My name is " + full_name)
print("My age is " + str(age))

# Typecasting
# int() - converts the value to integer value
# float() - converts the value into a float value
#str() - converts the value int string

print(int(3.5))
print(int("9861"))

#F-strings, add a lowercase "f" before the string and place variable in {}
print(f"My age is {age}")
print(f"Hi, my name is {full_name} and my age is {age}")

# Operations
# Arithmetic Operators - performs mathematical operations

print(1 + 10)
print(15 - 8)
print(18 * 9)
print(21 / 7)
print(18 % 4)
print(2 ** 6)

# Assignment Operators
num1 = 3
num1 += 4

# Comparison Operator - used to compare values (return a boolean value)
print(1 == 1)

# Logical Operators
print(not False)